package com.afpa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.afpa.conection.MyConnection;
import com.afpa.modele.Emp;

public class EmpImp implements Dao<Emp> {

	@Override
	public Emp ajout(Emp obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Emp update(Emp obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Emp affiche(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void getAll() {
		Connection c = MyConnection.getConnection();
		List<Emp> res = null;
		
		try {
			PreparedStatement select = c.prepareStatement("select * from emp");
			ResultSet result = select.executeQuery();
			
			while (result.next()) {
				int noemp = result.getInt("noemp");
				String nom = result.getString("nom");
				String prenom = result.getString("prenom");
				String emploi = result.getString("emploi");
				double salaire = result.getDouble("sal");
				System.out.println("numero employ : " + noemp +", nom : "+nom+", prenom : "+ prenom+", emploi : "+ emploi+", salaire : "+ salaire);
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
	}


}
