package com.afpa.dao;

import java.util.List;
/**
 * l'interface represente les fonctionnalites a implementer pour les tables emp et serv
 */
public interface Dao<T> {
	/**
	 * methode d'ajout, doit servir a ajouter des emp ou serv
	 */
	T ajout(T obj);
	/**
	 * methode de mise a jour, doit servir a mettre a jour un emp ou un serv
	 */
	T update(T obj);
	/**
	 * methode d'affichage doit permettre d'affiche un employee
	 */
	T affiche(int id);
	/**
	 * methode de listing doit permettre de lister les emp ou les serv
	 */
	void getAll();
	
}
