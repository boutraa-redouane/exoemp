package com.afpa.conection;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.postgresql.ds.PGPoolingDataSource;
import org.postgresql.ds.PGSimpleDataSource;

public class MyDataSourceFactory {
	
	public static DataSource getPGDataSource() {
		Properties props = new Properties();
		FileInputStream fis = null;
		PGSimpleDataSource source = new PGSimpleDataSource();
		try {
			fis = new FileInputStream("conf/db.properties");
			props.load(fis);
			source.setUrl(props.getProperty("url"));
//			source.setServerName();
			source.setUser(props.getProperty("username"));
			source.setPassword(props.getProperty("password"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return source;
	}
}
