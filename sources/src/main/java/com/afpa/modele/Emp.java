package com.afpa.modele;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * La classe Emp modelise un employe.
 * Chaque attibut represente une colonne de la table emp
 *
 * @author red
 */
@Getter
@Setter
public class Emp {

	/**
	 * le numero de l'employe
	 */
	private int noemp;
	/**
	 * le numero de service de l'employe
	 */
	private int noserv;
	/**
	 * le numero employe de son superieur hierarchique
	 */
	private int sup;
	/**
	 * le salaire de l'employe
	 */
	private double sal;
	/**
	 * la commission de l'employe
	 */
	private int comm;
	/**
	 * la date d'embauche de l'employe
	 */
	private Date embauche;
	/**
	 * le nom de l'employe
	 */
	private String nom;
	/**
	 * le prenom de l'employe
	 */
	private String prenom;
	/**
	 * le travail de l'employe
	 */
	private String emploi;
	
}
